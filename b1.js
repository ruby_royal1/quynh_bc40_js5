


// BÀI 1: QUẢN LÝ TUYỂN SINH
function tuyenSinh() {
    var dchuan = document.getElementById("txt-diem-chuan").value * 1;
    var a = document.getElementById('txt-khu-vuc').value;
    var b = document.getElementById('txt-doi-tuong').value;
    var m1 = document.getElementById("m1").value * 1;
    var m2 = document.getElementById("m2").value * 1;
    var m3 = document.getElementById("m3").value * 1;
    var dKhuVuc = 0;
    var dDoiTuong = 0;

    switch (a) {
        case 'a':
            dKhuVuc = 2
            break;
        case 'b':
            dKhuVuc = 1
            break;
        case 'c':
            dKhuVuc = 0.5;
            break;
        case 'x':
            dDoiTuong = 0;
            break;
        default:
            break;
    }
    switch (b) {
        case 'mot':
            dDoiTuong = 2.5
            break;
        case 'hai':
            dDoiTuong = 1.5
            break;
        case 'ba':
            dDoiTuong = 1;
            break;
        case 'x':
            dDoiTuong = 0;
            break;
        default:
            break;
    }
    var d = document.getElementById("txt-kq");
    var tong = m1 + m2 + m3 + dKhuVuc + dDoiTuong;
    (tong >= dchuan) ? d.innerHTML = `Bạn đã đậu! Tổng điểm: ${tong} điểm` : d.innerHTML = `Bạn đã rớt! Tổng điểm: ${tong} điểm`;
}

function tinhTienDien() {
    var tenEl = document.getElementById("txt-ten").value;
    var kw = document.getElementById("txt-kw").value * 1;
    var kw1 = 500;
    var kw2 = 650;
    var kw3 = 850;
    var kw4 = 1100;
    var kw5 = 1300;
    var tien = 0;
    if (kw <= 50) {
        tien = kw * kw1;
    } else if (kw > 50 && kw <= 100) {
        tien = 50 * kw1 + (kw - 50) * kw2;
    } else if (kw > 100 && kw <= 200) {
        tien = 50 * kw1 + 50 * kw2 + (kw - 100) * kw3;
    } else if (kw > 200 && kw <= 350) {
        tien = 50 * kw1 + 50 * kw2 + 100 * kw3 + (kw - 200) * kw4;
    } else {
        tien = 50 * kw1 + 50 * kw2 + 100 * kw3 + 150 * kw4 + (kw - 350) * kw5;
    }
    document.getElementById("txt-tien-dien").innerHTML = `Họ và tên: ${tenEl}; Tiền điện: ${tien} đồng`;


}
// bài 3: tính thuế thu nhập cá nhân
function tinhThue() {
    var tenEl = document.getElementById("txt-ho-va-ten").value;
    var tienEl = document.getElementById("num-tien-nam").value * 1;
    var soNguoiEl = document.getElementById("num-nguoi").value * 1;
    var muc1 = 0.05;
    var muc2 = 0.1;
    var muc3 = 0.15;
    var muc4 = 0.2;
    var muc5 = 0.25;
    var muc6 = 0.3;
    var muc7 = 0.35;
    var tienChiuThue = tienEl - 4 - soNguoiEl * 1.6;
    var thue = 0;
    t = 1000000;
    if (tienChiuThue <= 60 * t) {
        thue = tienChiuThue * muc1;
    } else if (tienChiuThue > 60 * t && tienChiuThue <= 120 * t) {
        thue = tienChiuThue * muc2;
    } else if (tienChiuThue > 120 * t && tienChiuThue <= 210 * t) {
        thue = tienChiuThue * muc3;
    } else if (tienChiuThue > 210 * t && tienChiuThue <= 384 * t) {
        thue = tienChiuThue * muc4;
    } else if (tienChiuThue > 384 * t && tienChiuThue <= 624 * t) {
        thue = tienChiuThue * muc5;
    } else if (tienChiuThue > 624 * t && tienChiuThue <= 960 * t) {
        thue = tienChiuThue * muc6;
    } else {
        thue = tienChiuThue * muc7;
    }
    document.getElementById("txt-tien-thue").innerHTML = `Họ và tên: ${tenEl}; Tiền thuế: ${Intl.NumberFormat('vn-VN').format(thue)} đồng`;

}

// BÀI 4: TÍNH TIỀN CÁP

function disInput(){
    var customerEl = document.getElementById('txt-customer').value;
    var showEl=document.getElementById('Enterprise');
    if (customerEl=="b"){
showEl.style.display="block";
    } else if(customerEl=="a"){
        showEl.style.display="none"; 
    } else{
        showEl.style.display="none"; 
    }
}

function costCable() {
    var cusCodeEl = document.getElementById("txt-cusCode").value;
    var customerEl = document.getElementById('txt-customer').value;
    var advChanEl = document.getElementById('num-adv-chan').value * 1;
    var numConnEl = document.getElementById('num-connect').value * 1;
    var feeBill = 4.5;
    var basicSe = 20.5;
    var advChan = 7.5;

    if (customerEl=='a'||customerEl=='b'){
        switch (customerEl) {
            case 'a':
                feeBill = 4.5;
                basicSe = 20.5;
                advChan = 7.5*advChanEl;
                break;
            case 'b':
                feeBill = 15;
                (numConnEl<=10)?(basicSe=75):(basicSe=75+((numConnEl-10)*5));
                advChan = 50*advChanEl;
                break;
                
            default:
                break;
        }
        var cost=feeBill+basicSe+advChan;
      
        document.getElementById("txt-cable").innerHTML = `Mã khách hàng: ${cusCodeEl}  có tiền cáp là:  ${new Intl.NumberFormat("en-US", { style: "currency", currency: "USD" }).format(cost)};
    }`
} else {
    alert('Vui lòng chọn khách hàng!!')
}
}